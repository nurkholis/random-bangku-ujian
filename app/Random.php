<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Random extends Model
{
    protected $table = 'random';
    protected $primaryKey = 'id_random';
    public $timestamps = false;
}
