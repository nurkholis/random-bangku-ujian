<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;
use App\Siswa;
use App\Kelas;
use App\Guru;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        
        $jumlah = ['jurusan' => Jurusan::count(), 'siswa' => Siswa::count(), 'kelas' => Kelas::count(), 'guru' => Guru::count()];
        return view('home.home', ['jumlah' => $jumlah]);
    }
}
