<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;
use App\Jurusan;
class KelasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $kelas = new Kelas;
        $data = $kelas
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'kelas.id_jurusan')
            ->orderBy('kelas.id_kelas', 'desc')
            ->paginate(10);
        //dd($data);
        return view('kelas.home', ['kelass' => $data]);
    }
    public function create()
    {
        $jurusans = Jurusan::get();
        //dd($jurusans);
        return view('kelas.form', ['jurusans' => $jurusans]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'id_jurusan'  => 'required',
            'nama_kelas'  => 'required|min:4|max:200'
        ]);
        $kelas = new Kelas;
        $kelas->id_jurusan  = $request->id_jurusan;
        $kelas->nama_kelas  = $request->nama_kelas;
        $kelas->save();
        return redirect('kelas');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        $jurusans = Jurusan::get();
        //dd($jurusans);
        return view('kelas.form', ['jurusans' => $jurusans, 'kelass' => $kelas->toArray()]);
        //return view('kelas.form', [$jurusan->toArray()]);
    }
    public function update(Request $request, $id)
    {
        $kelas = Kelas::find($id);
        $kelas->id_jurusan    = $request->id_jurusan;
        $kelas->nama_kelas    = $request->nama_kelas;
        $kelas->save();
        return redirect('kelas');
    }
    public function destroy($id)
    {
        $kelas = Kelas::find($id)->delete();
        return redirect('kelas');
    }
}
