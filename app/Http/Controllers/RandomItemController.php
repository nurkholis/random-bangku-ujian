<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RandomItem;
use App\Siswa;
use App\Random;
use App\Ruang;
use App\Kelas;
use Redirect;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class RandomItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($id_random)
    {
        $random = Random::find($id_random);
        $ruang = new Ruang;
        $ruangs = $ruang
            ->orderBy('id_ruang', 'desc') 
            ->get();
        //dd($random);
        $status_acak = RandomItem::where('id_random', '=', $id_random)->get()->toArray();
        $status_acak = count($status_acak);
        return view('randomitem.home',['random' => $random, 
            'ruangs' => $ruangs,
            'status_acak' => $status_acak]);
    }
    public function startRandom($id_random)
    {
        $random = Random::find($id_random);
        $siswa = new Siswa;
        $ruang = Ruang::get();
        $kelas = Kelas::get();
        $sisa_siswa = array();
        $siswa_kelas_terproses = array();
        foreach($kelas as $data){
            $siswa_kelas_terproses[$data->id_kelas] = 0;
        } 
        foreach($ruang as $data){
            $id_ruang = $data->id_ruang;
            $id_kelas_1 = $data->id_kelas_1;
            $id_kelas_2 = $data->id_kelas_2;
            $kapasitas_kelas_1 = $data->kapasitas_kelas_1;
            $kapasitas_kelas_2 = $data->kapasitas_kelas_2;
            $separuh_kapasitas_kelas_1 = floor($kapasitas_kelas_1/2);
            $separuh_kapasitas_kelas_2 = floor($kapasitas_kelas_2/2);
            //dd($separuh_kapasitas_kelas_1, $separuh_kapasitas_kelas_2);

            $siswa_kelas_pertama = DB::select("SELECT * FROM siswa WHERE id_kelas = '$id_kelas_1' LIMIT $siswa_kelas_terproses[$id_kelas_1], $kapasitas_kelas_1");
            $siswa_kelas_kedua = DB::select("SELECT * FROM siswa WHERE id_kelas = '$id_kelas_2' LIMIT $siswa_kelas_terproses[$id_kelas_2], $kapasitas_kelas_2");

           //dd($siswa_kelas_pertama);
            $nomor_bangku_1 = -3;
            $nomor_bangku_2 = -1;
            //random deret pertama
            
            echo "<br> ====== Ruang " .$data->nama_ruang. " ====== <br>";
            echo "<br> ====== Jurusan pertama ====== <br>";
            foreach ($siswa_kelas_pertama as $key => $value) {
                if($key < $separuh_kapasitas_kelas_1){
                    $bangku = $nomor_bangku_1 = $nomor_bangku_1 + 4;
                    echo '<br> # ' . $key . ' bangku ' . ($bangku) . ' => ' . $value->nama_siswa;
                    $this->simpanRandom($id_random, $value->nis, $id_ruang, $bangku);
                }
                else{
                    $bangku = $nomor_bangku_2 = $nomor_bangku_2 + 4;
                    echo '<br> # ' . $key . ' bangku ' . ($bangku) . ' => ' . $value->nama_siswa;
                    $this->simpanRandom($id_random, $value->nis, $id_ruang, $bangku);
                }
            }
            // random deret kedua
            $nomor_bangku_1 = -2;
            $nomor_bangku_2 = 0;
            echo "<br> ====== Jurusan kedua ====== <br>";
            foreach ($siswa_kelas_kedua as $key => $value) {
                if($key <  $separuh_kapasitas_kelas_2){
                    $bangku = $nomor_bangku_1 = $nomor_bangku_1 + 4;
                    echo '<br> # ' . $key . ' bangku ' . ($bangku) . ' => ' . $value->nama_siswa;
                    $this->simpanRandom($id_random, $value->nis, $id_ruang, $bangku);
                }
                else{
                    $bangku = $nomor_bangku_2 = $nomor_bangku_2 + 4;
                    echo '<br> # ' . $key . ' bangku ' . ($bangku) . ' => ' . $value->nama_siswa;
                    $this->simpanRandom($id_random, $value->nis, $id_ruang, $bangku);
                }
            }
            echo '<br>';
            $siswa_kelas_terproses[$id_kelas_1] += $kapasitas_kelas_1;
            $siswa_kelas_terproses[$id_kelas_2] += $kapasitas_kelas_2;
            echo "<br> ====siswa kelas terproses ==== <br> ";
            print_r($siswa_kelas_terproses);
        } // akhir loop ruang
        
        return redirect('/random/item/'.$id_random);
    }
    public function simpanRandom($id_random, $nis, $id_ruang, $bangku){
        $random_item = new RandomItem;
        $random_item->id_random     = $id_random;
        $random_item->nis      = $nis;
        $random_item->id_ruang      = $id_ruang;
        $random_item->bangku        = $bangku;
        $random_item->save();
    }

    public function destroyRandom($id_random)
    {
        //dd($id_random);
        RandomItem::where('id_random', $id_random)->delete();
        return redirect('/random/item/'.$id_random);
    }
    public function showRandom($id_random, $id_ruang)
    {
        $ruang = Ruang::find($id_ruang);
        $random = Random::find($id_random);
        $random_ruang = RandomItem::
            where('id_random', $id_random)
            ->where('id_ruang', $id_ruang)
            ->join('siswa', 'siswa.nis', '=', 'random_item.nis')
            ->join('kelas', 'kelas.id_kelas', '=', 'siswa.id_kelas')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'kelas.id_jurusan')
            ->orderBy('id_random_item')
            ->get();
        //dd($ruang);
        return view('randomitem.show', ['ruang' => $ruang,
            'random' => $random,
            'random_ruang' => $random_ruang]);
    }
    public function printRandom($id_random, $id_ruang)
    {
        $ruang = Ruang::find($id_ruang);
        $random = Random::find($id_random);
        $random_ruang = RandomItem::
            where('id_random', $id_random)
            ->where('id_ruang', $id_ruang)
            ->join('siswa', 'siswa.id_siswa', '=', 'random_item.id_siswa')
            ->join('kelas', 'kelas.id_kelas', '=', 'siswa.id_kelas')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'kelas.id_jurusan')
            ->orderBy('id_random_item')
            ->get();
        //dd($random_ruang);
        foreach($random_ruang as $data){
            echo('
                <tr>
                    <td>'. $data->nama_siswa .'</td>
                    <td>'. $data->bangku .'</td>
                    <td>'. $data->nama_jurusan .'</td>
                    <td>'. $data->nama_kelas .'</td>
                    <td>'.'</td>
                </tr>
            ');
        }
    }
    
}
