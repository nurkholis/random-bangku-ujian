<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
    public function index(){
        // $cd = new Cd;
        // $cds = $cd->get();
        // config(['app.locale' => 'id']);
        // return view('front', ['cds' => $cds]);
        $student = new Student;
        $students = $student->get();
        dd($students);
    }
    public function create(){
        //
    }
    public function store(Request $request){   
        // coba dengan variabel global
        $validate_numeric = 'required|numeric';
        $validateData = $request->validate([
            'name'      => 'required|min:4|max:200',
            'age'       => $validate_numeric,
            'majors_id' => $validate_numeric,
            'classes_id'=> $validate_numeric,
            'address'   => 'required|min:4',
            'gender'    => 'required',
        ],[
            'name.required'      => 'Nama harus diisi',
            'name.min:4'         => 'Nama minimal 4 karakter',
            'name.max:200'       => 'Nama maksimal 200 karakter'
        ]);

        $student = new Student;
        $student->classes_id    = $request->classes_id;
        $student->majors_id     = $request->majors_id;
        $student->name          = $request->name;
        $student->gender        = $request->gender;
        $student->addres       = $request->address;
        $student->age           = $request->age;
        dd($student->save());
        //return redirect('/');
    }
    public function show($id){
        //
    }
    public function edit($id){
        //
    }
    public function update(Request $request, $id){
        //harrus ada validasi
        $student = Student::find($id);
        $student->classes_id    = $request->classes_id;
        $student->majors_id     = $request->majors_id;
        $student->name          = $request->name;
        $student->gender        = $request->gender;
        $student->addres       = $request->address;
        $student->age           = $request->age;
        $student->save();
    }
    public function destroy($id){
        //
    }
}
