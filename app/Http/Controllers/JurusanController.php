<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;
class JurusanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $jurusan = new Jurusan;
        $data = $jurusan
            ->orderBy('id_jurusan', 'desc') 
            ->paginate(2);
        return view('jurusan.home', ['jurusans' => $data]);
    }
    public function create()
    {
        return view('jurusan.form');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama_jurusan'  => 'required|min:4|max:200'
        ]);
        $jurusan = new Jurusan;
        $jurusan->nama_jurusan       = $request->nama_jurusan;
        $jurusan->save();
        return redirect('jurusan');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $jurusan = Jurusan::find($id);
        return view('jurusan.form', $jurusan->toArray());
    }
    public function update(Request $request, $id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->nama_jurusan       = $request->nama_jurusan;
        $jurusan->save();
        return redirect('jurusan');
    }
    public function destroy($id)
    {
        $jurusan = Jurusan::find($id);
        $jurusan->delete();
        return redirect('jurusan');
    }
}
