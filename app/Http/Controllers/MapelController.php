<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mapel;
use App\Jurusan;
class MapelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $mapel = new Mapel;
        $data = $mapel
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'mapel.id_jurusan')
            ->orderBy('mapel.id_jurusan', 'desc')
            ->paginate(10);
        //dd($data);
        return view('mapel.home', ['mapel' => $data]);
    }
    public function create()
    {
        $jurusans = Jurusan::get();
        //dd($jurusans);
        return view('mapel.form', ['jurusans' => $jurusans]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'id_jurusan'  => 'required',
            'nama_mapel'  => 'required|max:200'
        ]);
        $mapel = new Mapel;
        $mapel->id_jurusan  = $request->id_jurusan;
        $mapel->nama_mapel  = $request->nama_mapel;
        $mapel->save();
        return redirect('mapel');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        $jurusans = Jurusan::get();
        //dd($jurusans);
        return view('kelas.form', ['jurusans' => $jurusans, 'kelass' => $kelas->toArray()]);
        //return view('kelas.form', [$jurusan->toArray()]);
    }
    public function update(Request $request, $id)
    {
        $kelas = Kelas::find($id);
        $kelas->id_jurusan    = $request->id_jurusan;
        $kelas->nama_kelas    = $request->nama_kelas;
        $kelas->save();
        return redirect('kelas');
    }
    public function destroy($id)
    {
        $kelas = Kelas::find($id)->delete();
        return redirect('kelas');
    }
}
