<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guru;
class GuruController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $guru = new Guru;
        $data = $guru
            ->orderBy('id_guru', 'desc')    
            ->paginate(2);
        return view('guru.home', ['gurus' => $data]);
    }
    public function create()
    {
        return view('guru.form');
    }
    public function store(Request $request)
    {
        $request->validate([
            'id_guru' => 'required',
            'nama_guru'    => 'required|min:4|max:200',
            'jenis_kelamin' => 'required',
            'tempat_lahir'  => 'required|min:4|max:200',
            'tanggal_lahir' => 'required',
            'alamat'        => 'required|min:4',
        ]);
        $guru = new Guru;
        $guru->id_guru         = $request->id_guru;
        $guru->nama_guru       = $request->nama_guru;
        $guru->jenis_kelamin   = $request->jenis_kelamin;
        $guru->tempat_lahir    = $request->tempat_lahir;
        $guru->tanggal_lahir   = $request->tanggal_lahir;
        $guru->alamat          = $request->alamat;
        $guru->save();
        return redirect('guru');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $guru = Guru::find($id);
        return view('guru.form', ['guru' => $guru->toArray()]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_guru' => 'required',
            'nama_guru'    => 'required|min:4|max:200',
            'jenis_kelamin' => 'required',
            'tempat_lahir'  => 'required|min:4|max:200',
            'tanggal_lahir' => 'required',
            'alamat'        => 'required|min:4',
        ]);

        $guru = Guru::find($id);
        $guru->id_guru         = $request->id_guru;
        $guru->nama_guru       = $request->nama_guru;
        $guru->jenis_kelamin   = $request->jenis_kelamin;
        $guru->tempat_lahir    = $request->tempat_lahir;
        $guru->tanggal_lahir   = $request->tanggal_lahir;
        $guru->alamat          = $request->alamat;
        $guru->save();
        return redirect('guru');
    }
    public function destroy($id)
    {
        $guru = Guru::find($id)->delete();
        return redirect('guru');
    }
}
