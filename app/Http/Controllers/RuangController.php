<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruang;
Use App\Guru;
Use App\Kelas;
use Illuminate\Support\Facades\DB;

class RuangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $kelas = DB::select('SELECT *, (SELECT SUM(kapasitas_kelas_1) FROM ruang WHERE ruang.id_kelas_1 = kelas.id_kelas ) AS terpakai_1, (SELECT SUM(kapasitas_kelas_2) FROM ruang WHERE ruang.id_kelas_2 = kelas.id_kelas ) AS terpakai_2, (SELECT COUNT(nis) FROM siswa WHERE siswa.id_kelas = kelas.id_kelas) AS jumlah_siswa FROM kelas');
        //dd($kelas);
        $data = DB::select('SELECT *, k1.nama_kelas AS nama_kelas_1, k2.nama_kelas AS nama_kelas_2 FROM ruang r JOIN kelas k1 JOIN kelas k2 WHERE k1.id_kelas = r.id_kelas_1 AND k2.id_kelas = r.id_kelas_2');
        //dd($data);
        return view('ruang.home', 
            [
                'ruangs' => $data,
                'kelass' => $kelas,
            ]
        );
    }
    public function create()
    {
        $guru = Guru::get();
        $kelas = Kelas::get();
        //dd($kelas);
        return view('ruang.form', 
        [
            'gurus' => $guru,
            'kelass' => $kelas
        ]);
    }
    public function store(Request $request)
    {
        //dd($request->id_guru);
        $request->validate([
            'nama_ruang'  => 'required|min:4|max:200',
            'kapasitas_kelas_1'  => 'required|max:3',
            'kapasitas_kelas_2'  => 'required|max:3'
        ]);
        $ruang = new Ruang;
        $ruang->nama_ruang  = $request->nama_ruang;
        $ruang->id_kelas_1  = $request->id_kelas_1;
        $ruang->id_kelas_2  = $request->id_kelas_2;
        $ruang->kapasitas_kelas_1  = $request->kapasitas_kelas_1;
        $ruang->kapasitas_kelas_2  = $request->kapasitas_kelas_2;
        $ruang->save();
        return redirect('ruang');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $ruang = Ruang::find($id);
        $guru = Guru::get();
        $kelas = Kelas::get();
        return view('ruang.form', 
        [
            'gurus' => $guru, 
            'kelass' => $kelas,
            'ruang' => $ruang->toArray()
        ]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_ruang'  => 'required|min:4|max:200',
            'kapasitas_kelas_1'  => 'required|max:3',
            'kapasitas_kelas_2'  => 'required|max:3'
        ]);
        $ruang = Ruang::find($id);
        $ruang->nama_ruang  = $request->nama_ruang;
        $ruang->id_kelas_1  = $request->id_kelas_1;
        $ruang->id_kelas_2  = $request->id_kelas_2;
        $ruang->kapasitas_kelas_1  = $request->kapasitas_kelas_1;
        $ruang->kapasitas_kelas_2  = $request->kapasitas_kelas_2;
        $ruang->save();
        return redirect('ruang');
    }
    public function destroy($id)
    {
        Ruang::find($id)->delete();
        return redirect('ruang');
    }
}
