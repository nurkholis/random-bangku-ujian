<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Random;
use App\Helper;

class RandomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $random = new Random;
        $data = $random
            ->orderBy('id_random', 'desc')    
            ->paginate(2);
        return view('random.home', ['randoms' => $data]);
    }
    public function create()
    {
        return view('random.form');
    }
    public function store(Request $request)
    {
        $tanggal_sekarang = date('Y-m-d');
        $request->validate([
            'nama_random'    => 'required|min:4|max:200',
        ]);
        $random = new Random;
        $random->nama_random         = $request->nama_random;
        $random->tanggal_random      = $tanggal_sekarang;
        $random->save();
        return redirect('random');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $random = Random::find($id);
        return view('random.form', ['random' => $random->toArray()]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_random'    => 'required|min:4|max:200',
            'tanggal_random' => 'required'
        ]);
        $random = Random::find($id);
        $random->nama_random         = $request->nama_random;
        $random->tanggal_random      = $request->tanggal_random;
        $random->save();
        return redirect('random');
    }
    public function destroy($id)
    {
        Random::find($id)->delete();
        return redirect('random');
    }
}
