<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\Kelas;
use Excel;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $siswa = new Siswa;
        $data = $siswa
            ->join('kelas', 'kelas.id_kelas', '=', 'siswa.id_kelas')
            ->join('jurusan', 'jurusan.id_jurusan', '=', 'kelas.id_jurusan')
            ->orderBy('siswa.nis', 'desc')
            ->paginate(20);
        //dd($data);
        return view('siswa.home', ['siswas' => $data]);
    }
    public function create()
    {
        $kelass = Kelas::get();
        //dd($kelass);
        return view('siswa.form', ['kelass' => $kelass]);
    }
    public function store(Request $request)
    {
        
        $request->validate([
            'nis'      => 'required',
            'id_kelas'      => 'required',
            'nama_siswa'    => 'required|min:4|max:200',
            'jenis_kelamin' => 'required',
            'tempat_lahir'  => 'required|min:4|max:200',
            'tanggal_lahir' => 'required',
            'alamat'        => 'required|min:4',
        ]);
        $siswa = new Siswa;
        $siswa->nis             = $request->nis;
        $siswa->id_kelas        = $request->id_kelas;
        $siswa->nama_siswa      = $request->nama_siswa;
        $siswa->jenis_kelamin   = $request->jenis_kelamin;
        $siswa->tempat_lahir    = $request->tempat_lahir;
        $siswa->tanggal_lahir   = $request->tanggal_lahir;
        $siswa->alamat          = $request->alamat;
        $siswa->save();
        return redirect('siswa');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $siswa = Siswa::find($id);
        $kelass = kelas::get();
        //dd($siswa);
        return view('siswa.form', ['kelass' => $kelass, 'siswa' => $siswa->toArray()]);
        //return view('kelas.form', [$jurusan->toArray()]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nis'      => 'required',
            'id_kelas'      => 'required',
            'nama_siswa'    => 'required|min:4|max:200',
            'jenis_kelamin' => 'required',
            'tempat_lahir'  => 'required|min:4|max:200',
            'tanggal_lahir' => 'required',
            'alamat'        => 'required|min:4',
        ]);
        $siswa = Siswa::find($id);
        $siswa->nis        = $request->nis;
        $siswa->id_kelas        = $request->id_kelas;
        $siswa->nama_siswa      = $request->nama_siswa;
        $siswa->jenis_kelamin   = $request->jenis_kelamin;
        $siswa->tempat_lahir    = $request->tempat_lahir;
        $siswa->tanggal_lahir   = $request->tanggal_lahir;
        $siswa->alamat          = $request->alamat;
        $siswa->save();
        return redirect('siswa');
    }
    public function destroy($id)
    {
        $siswa = Siswa::find($id)->delete();
        return redirect('siswa');
    }
    public function export()
    {
        $siswa = Siswa::select('nis', 'id_kelas', 'nama_siswa', 'jenis_kelamin', 'tanggal_lahir', 'tempat_lahir', 'alamat')->get();
        return Excel::create('data_siswa', function($excel) use ($siswa){
            $excel->sheet('mysheet', function($sheet) use ($siswa){
                $sheet->fromArray($siswa);
            });
        })->download('xls');
    }
    public function import(Request $request)
    {
        //dd('hai');
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path, function($reader){})->get();
            if(!empty($data) && $data->count()){
                foreach($data as $key => $value){
                    $siswa                  = new Siswa;
                    $siswa->nis        = $value->nis;
                    $siswa->id_kelas        = $value->id_kelas;
                    $siswa->nama_siswa      = $value->nama_siswa;
                    $siswa->jenis_kelamin   = $value->jenis_kelamin;
                    $siswa->tempat_lahir    = $value->tempat_lahir;
                    $siswa->tanggal_lahir   = $value->tanggal_lahir;
                    $siswa->alamat          = $value->alamat;
                    $siswa->save();
                }
            }
        }
        return back();
    }
}
