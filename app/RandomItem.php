<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RandomItem extends Model
{
    protected $table = 'random_item';
    protected $primaryKey = 'id_random_item';
    public $timestamps = false;
}
