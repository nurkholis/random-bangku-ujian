<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/log', function(){
    return view('auth.log');
});
Route::get('/logout', function(){
    Auth::logout();
    return view ('auth.login');
});

Route::get('/student', 'StudentController@index');
Route::post('/student', 'StudentController@store');
Route::put('/student/{id}', 'StudentController@update');

Route::prefix('jurusan')->group( function() {
    Route::get('/', 'JurusanController@index');
    Route::get('/form', 'JurusanController@create');
    Route::get('/form/{id}', 'JurusanController@edit');
    Route::post('/', 'JurusanController@store');
    Route::get('/delete/{id}', 'JurusanController@destroy');
    Route::put('/{id}', 'JurusanController@update');
});
Route::prefix('kelas')->group( function() {
    Route::get('/', 'KelasController@index');
    Route::get('/form', 'KelasController@create');
    Route::get('/form/{id}', 'KelasController@edit');
    Route::post('/', 'KelasController@store');
    Route::get('/delete/{id}', 'KelasController@destroy');
    Route::put('/{id}', 'KelasController@update');
});
Route::prefix('siswa')->group( function() {
    Route::get('/', 'SiswaController@index');
    Route::get('/form', 'SiswaController@create');
    Route::get('/export', 'SiswaController@export')->name("export");
    Route::post('/import', 'SiswaController@import')->name("import");
    Route::get('/form/{id}', 'SiswaController@edit');
    Route::post('/', 'SiswaController@store');
    Route::get('/delete/{id}', 'SiswaController@destroy');
    Route::put('/{id}', 'SiswaController@update');
});
Route::prefix('guru')->group( function() {
    Route::get('/', 'GuruController@index');
    Route::get('/form', 'GuruController@create');
    Route::get('/form/{id}', 'GuruController@edit');
    Route::post('/', 'GuruController@store');
    Route::get('/delete/{id}', 'GuruController@destroy');
    Route::put('/{id}', 'GuruController@update');
});
Route::prefix('ruang')->group( function() {
    Route::get('/', 'RuangController@index');
    Route::get('/form', 'RuangController@create');
    Route::get('/form/{id}', 'RuangController@edit');
    Route::post('/', 'RuangController@store');
    Route::get('/delete/{id}', 'RuangController@destroy');
    Route::put('/{id}', 'RuangController@update');
});
Route::prefix('random')->group( function() {
    Route::get('/', 'RandomController@index');
    Route::get('/form', 'RandomController@create');
    Route::get('/form/{id}', 'RandomController@edit');
    Route::post('/', 'RandomController@store');
    Route::get('/delete/{id}', 'RandomController@destroy');
    Route::put('/{id}', 'RandomController@update');
});
Route::prefix('random/item')->group( function() {
    Route::get('/{id_random}', 'RandomItemController@index');
    Route::get('/start/{id_random}', 'RandomItemController@startRandom');
    Route::get('/destroy/{id_random}', 'RandomItemController@destroyRandom');
    Route::get('/show/{id_random}/{id_ruang}', 'RandomItemController@showRandom');
    Route::get('/print/{id_random}/{id_ruang}', 'RandomItemController@printRandom');
});

Route::prefix('mapel')->group( function() {
    Route::get('/', 'MapelController@index');
    Route::get('/form', 'MapelController@create');
    Route::get('/form/{id}', 'MapelController@edit');
    Route::post('/', 'MapelController@store');
    Route::get('/delete/{id}', 'MapelController@destroy');
    Route::put('/{id}', 'MapelController@update');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
