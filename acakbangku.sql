-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Jul 2018 pada 04.52
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acakbangku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` varchar(200) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tempat_lahir` varchar(200) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`) VALUES
(123456789, 'Ahmad Sanusi', 'L', 'Sampang', '2018-05-23', 'Kadungdung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jam`
--

CREATE TABLE `jam` (
  `nama_jam` varchar(200) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'TKJ (Teknik Komputer Jaringan)'),
(2, 'TM (Teknik Mesin)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `nama_kelas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_jurusan`, `nama_kelas`) VALUES
(1, 1, '1 TKJ'),
(2, 1, '2 TKJ'),
(3, 1, '3 TKJ'),
(4, 1, '1 TM'),
(5, 1, '2 TM'),
(6, 1, '3 TM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `nama_mapel` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `id_jurusan`, `nama_mapel`) VALUES
(1, 1, 'Matematika'),
(2, 1, 'Bahasa Indonesia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `random`
--

CREATE TABLE `random` (
  `id_random` int(11) NOT NULL,
  `nama_random` varchar(200) NOT NULL,
  `tanggal_random` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `random`
--

INSERT INTO `random` (`id_random`, `nama_random`, `tanggal_random`) VALUES
(1, 'coba', '2018-05-29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `random_item`
--

CREATE TABLE `random_item` (
  `id_random_item` int(11) NOT NULL,
  `id_random` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `bangku` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `random_item`
--

INSERT INTO `random_item` (`id_random_item`, `id_random`, `nis`, `id_ruang`, `bangku`) VALUES
(81, 1, 1, 1, '1'),
(82, 1, 2, 1, '5'),
(83, 1, 3, 1, '9'),
(84, 1, 4, 1, '13'),
(85, 1, 5, 1, '17'),
(86, 1, 6, 1, '3'),
(87, 1, 7, 1, '7'),
(88, 1, 8, 1, '11'),
(89, 1, 9, 1, '15'),
(90, 1, 10, 1, '19'),
(91, 1, 21, 1, '2'),
(92, 1, 22, 1, '6'),
(93, 1, 23, 1, '10'),
(94, 1, 24, 1, '14'),
(95, 1, 25, 1, '18'),
(96, 1, 26, 1, '4'),
(97, 1, 27, 1, '8'),
(98, 1, 28, 1, '12'),
(99, 1, 29, 1, '16'),
(100, 1, 30, 1, '20'),
(101, 1, 11, 2, '1'),
(102, 1, 12, 2, '5'),
(103, 1, 13, 2, '9'),
(104, 1, 14, 2, '13'),
(105, 1, 15, 2, '17'),
(106, 1, 16, 2, '3'),
(107, 1, 17, 2, '7'),
(108, 1, 18, 2, '11'),
(109, 1, 19, 2, '15'),
(110, 1, 20, 2, '19'),
(111, 1, 31, 2, '2'),
(112, 1, 32, 2, '6'),
(113, 1, 33, 2, '10'),
(114, 1, 34, 2, '14'),
(115, 1, 35, 2, '4'),
(116, 1, 36, 2, '8'),
(117, 1, 37, 2, '12'),
(118, 1, 38, 2, '16'),
(119, 1, 39, 2, '20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `id_kelas_1` int(11) NOT NULL,
  `id_kelas_2` int(11) NOT NULL,
  `nama_ruang` varchar(200) NOT NULL,
  `kapasitas_kelas_1` int(11) NOT NULL,
  `kapasitas_kelas_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `id_kelas_1`, `id_kelas_2`, `nama_ruang`, `kapasitas_kelas_1`, `kapasitas_kelas_2`) VALUES
(1, 1, 4, 'wiro', 10, 10),
(2, 1, 4, 'sableng', 10, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nama_siswa` varchar(200) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tempat_lahir` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nis`, `id_kelas`, `nama_siswa`, `jenis_kelamin`, `tanggal_lahir`, `tempat_lahir`, `alamat`) VALUES
(1, 1, 'Ridwan 1 TKJ', 'L', '2018-03-30', 'Sampang', 'madura'),
(2, 1, 'Junaidi TKJ', 'L', '2018-04-18', 'Sampang', 'kanginan'),
(3, 1, 'Badrut TKJ', 'L', '2018-04-18', 'bangkalan', 'batu poro'),
(4, 1, 'Sulis TKJ', 'P', '2018-04-17', 'Malang', 'Sampang'),
(5, 1, 'Leha TKJ', 'P', '2018-04-18', 'Bandung', 'bandung City'),
(6, 1, 'Rohman TKJ', 'P', '2018-04-24', 'Sampang', 'Madura city'),
(7, 1, 'faisal TKJ', 'L', '2018-04-25', 'Malang', 'sampang kdd'),
(8, 1, 'Dedi TKJ', 'L', '2018-03-28', 'sampang kota', 'sampang'),
(9, 1, 'Fendi TKJ', 'L', '2018-04-05', 'malang', 'Sampang'),
(10, 1, 'Hendra TKJ', 'L', '2018-04-26', 'bandung', 'Jalan delima'),
(11, 1, 'Munayyeh TKJ', 'P', '2018-04-10', 'Sampang', 'Sampang'),
(12, 1, 'Dewi TKJ', 'P', '2018-04-09', 'Aing sareh', 'sampang bahari'),
(13, 1, 'Puspita TKJ', 'P', '2018-04-24', 'Camplong', 'camplong city'),
(14, 1, 'Fendi bebrian TKJ', 'L', '2018-04-05', 'Sampang kota', 'Sampang madura'),
(15, 1, 'Bastian TKJ', 'L', '2018-04-10', 'Karongan', 'Madura jaya'),
(16, 1, 'Aisah TKJ', 'P', '2018-04-24', 'Sampang', 'Rajawali'),
(17, 1, 'Endang TKJ', 'P', '2018-05-05', 'Sampang', 'Ajigunung sampang'),
(18, 1, 'Sulastri TKJ', 'P', '2018-04-14', 'Sampang', 'Sampang jaya'),
(19, 1, 'Iwan TKJ', 'L', '2018-04-17', 'Mojokerto', 'Sampang'),
(20, 1, 'Roni TKJ', 'L', '2018-04-17', 'Pamekasan', 'sampang'),
(21, 4, 'Ridwan TSM', 'L', '2018-03-30', 'Sampang', 'madura'),
(22, 4, 'Junaidi TSM', 'L', '2018-04-18', 'Sampang', 'kanginan'),
(23, 4, 'Badrut TSM', 'L', '2018-04-18', 'bangkalan', 'batu poro'),
(24, 4, 'Sulis TSM', 'P', '2018-04-17', 'Malang', 'Sampang'),
(25, 4, 'Leha TSM', 'P', '2018-04-18', 'Bandung', 'bandung City'),
(26, 4, 'Rohman TSM', 'P', '2018-04-24', 'Sampang', 'Madura city'),
(27, 4, 'faisal TSM', 'L', '2018-04-25', 'Malang', 'sampang kdd'),
(28, 4, 'Dedi TSM', 'L', '2018-03-28', 'sampang kota', 'sampang'),
(29, 4, 'Fendi TSM', 'L', '2018-04-05', 'malang', 'Sampang'),
(30, 4, 'Hendra TSM', 'L', '2018-04-26', 'bandung', 'Jalan delima'),
(31, 4, 'Munayyeh TSM', 'P', '2018-04-10', 'Sampang', 'Sampang'),
(32, 4, 'Dewi TSM', 'P', '2018-04-09', 'Aing sareh', 'sampang bahari'),
(33, 4, 'Puspita TSM', 'P', '2018-04-24', 'Camplong', 'camplong city'),
(34, 4, 'Fendi bebrian TSM', 'L', '2018-04-05', 'Sampang kota', 'Sampang madura'),
(35, 4, 'Bastian TSM', 'L', '2018-04-10', 'Karongan', 'Madura jaya'),
(36, 4, 'Aisah TSM', 'P', '2018-04-24', 'Sampang', 'Rajawali'),
(37, 4, 'Endang TSM', 'P', '2018-05-05', 'Sampang', 'Ajigunung sampang'),
(38, 4, 'Sulastri TSM', 'P', '2018-04-14', 'Sampang', 'Sampang jaya'),
(39, 4, 'Iwan TSM', 'L', '2018-04-17', 'Mojokerto', 'Sampang'),
(40, 4, 'Roni TSM', 'L', '2018-04-17', 'Pamekasan', 'sampang'),
(41, 2, 'Ridwan TSM', 'L', '2018-03-30', 'Sampang', 'madura'),
(42, 2, 'Junaidi TSM', 'L', '2018-04-18', 'Sampang', 'kanginan'),
(43, 2, 'Badrut TSM', 'L', '2018-04-18', 'bangkalan', 'batu poro'),
(44, 2, 'Anis', 'P', '2018-04-17', 'Malang', 'Sampang'),
(45, 2, 'Soheh', 'P', '2018-04-18', 'Bandung', 'bandung City'),
(46, 2, 'Latifatul Mutmainnah', 'P', '2018-04-24', 'Sampang', 'Madura city'),
(47, 2, 'Fausiya', 'L', '2018-04-25', 'Malang', 'sampang kdd'),
(48, 2, 'Deny Firmasyah', 'L', '2018-03-28', 'sampang kota', 'sampang'),
(49, 2, 'Helmi Muharromi', 'L', '2018-04-05', 'malang', 'Sampang'),
(50, 2, 'Fajar Ridwa', 'L', '2018-04-26', 'bandung', 'Jalan delima'),
(51, 2, 'Hidayatur Fitriyah', 'P', '2018-04-10', 'Sampang', 'Sampang'),
(52, 2, 'Nur Aini', 'P', '2018-04-09', 'Aing sareh', 'sampang bahari'),
(53, 2, 'Layla', 'P', '2018-04-24', 'Camplong', 'camplong city'),
(54, 2, 'ABD. Syukur', 'L', '2018-04-05', 'Sampang kota', 'Sampang madura'),
(55, 2, 'Umarul Faruk', 'L', '2018-04-10', 'Karongan', 'Madura jaya'),
(56, 2, 'Anas Shobirin', 'P', '2018-04-24', 'Sampang', 'Rajawali'),
(57, 2, 'Agus Maulidi', 'P', '2018-05-05', 'Sampang', 'Ajigunung sampang'),
(58, 2, 'Aina Kurniawati', 'P', '2018-04-14', 'Sampang', 'Sampang jaya'),
(59, 2, 'Fathor Rahman', 'L', '2018-04-17', 'Mojokerto', 'Sampang'),
(60, 2, 'Saiful Bahri', 'L', '2018-04-17', 'Pamekasan', 'sampang'),
(61, 5, 'Bakriyadi', 'L', '2018-03-30', 'Sampang', 'madura'),
(62, 5, 'Hermansyah', 'L', '2018-04-18', 'Sampang', 'kanginan'),
(63, 5, 'Misriyadi', 'L', '2018-04-18', 'bangkalan', 'batu poro'),
(64, 5, 'Rahmatul Yasirah', 'P', '2018-04-17', 'Malang', 'Sampang'),
(65, 5, 'Quratul Aini', 'P', '2018-04-18', 'Bandung', 'bandung City'),
(66, 5, 'Rozana El-Imani', 'P', '2018-04-24', 'Sampang', 'Madura city'),
(67, 5, 'Amilan Aziza', 'L', '2018-04-25', 'Malang', 'sampang kdd'),
(68, 5, 'Taufiqur Rahman', 'L', '2018-03-28', 'sampang kota', 'sampang'),
(69, 5, 'ABD Rahman', 'L', '2018-04-05', 'malang', 'Sampang'),
(70, 5, 'Zainur Rahman', 'L', '2018-04-26', 'bandung', 'Jalan delima'),
(71, 5, 'Rohemah', 'P', '2018-04-10', 'Sampang', 'Sampang'),
(72, 5, 'Sumiyana', 'P', '2018-04-09', 'Aing sareh', 'sampang bahari'),
(73, 5, 'Wasilatur Rahmi', 'P', '2018-04-24', 'Camplong', 'camplong city'),
(74, 5, 'ABD. Syukur', 'L', '2018-04-05', 'Sampang kota', 'Sampang madura'),
(75, 5, 'Rifqi Efendi', 'L', '2018-04-10', 'Karongan', 'Madura jaya'),
(76, 5, 'Aini Halimiyah', 'P', '2018-04-24', 'Sampang', 'Rajawali'),
(77, 5, 'Uswatun Hasanah', 'P', '2018-05-05', 'Sampang', 'Ajigunung sampang'),
(78, 5, 'Karimatul Jannah', 'P', '2018-04-14', 'Sampang', 'Sampang jaya'),
(79, 5, 'Kholilur Rahman', 'L', '2018-04-17', 'Mojokerto', 'Sampang'),
(80, 5, 'MOH Haris', 'L', '2018-04-17', 'Pamekasan', 'sampang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nur kholis', '0', 'slamet@gmail.com', '$2y$10$clkRKsDp3Pya72JhGF2XFu9ZZGxrntFrJUvYttqsdLjIusz16uMeu', NULL, NULL, NULL),
(2, 'nur kholis', '0', 'user@gmail.com', '8080', 'P3lIlUQzYUIcrL3Ud0cy0z75vlLF1hW55RU0TE2yG0cTSG1Te0HQKFyqufwW', '2017-09-27 05:30:21', '2017-09-27 05:30:21'),
(3, 'nur kholis', 'nurkholis', 'nurkholispragaan@gmail.com', '$2y$10$clkRKsDp3Pya72JhGF2XFu9ZZGxrntFrJUvYttqsdLjIusz16uMeu', 'oHaLzwf0MRPqGXBQEcx6IYoA9Pc83Vqv4gZkXdk1JSJeZzK8O2mTyZPgFzhF', '2018-02-18 00:29:29', '2018-02-18 00:29:29'),
(4, 'Slamet', 'riadi', 'slamet@gmail.com', '12345', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `random`
--
ALTER TABLE `random`
  ADD PRIMARY KEY (`id_random`);

--
-- Indexes for table `random_item`
--
ALTER TABLE `random_item`
  ADD PRIMARY KEY (`id_random_item`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123456790;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `random`
--
ALTER TABLE `random`
  MODIFY `id_random` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `random_item`
--
ALTER TABLE `random_item`
  MODIFY `id_random_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
