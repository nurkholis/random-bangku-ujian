@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-graduation-cap"></i> Guru</h1>
            <p>Beranda Guru</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <!-- tabel -->
        <div class="col-md-12">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Guru</h3></div>
                    <div class="col-lg-6"><a href="/guru/form" class="btn btn-primary pull-right" type="button">Tambah</a></div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>JNS Kelamin</th>
                            <th>Alamat</th>
                            <th>Lahir</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($gurus as $data)
                            <tr>
                                <td>{{ $data->nama_guru }}</td>
                                <td>{{ $data->jenis_kelamin }}</td>
                                <td>{{ $data->alamat }}</td>
                                <td>{{ $data->tempat_lahir }}, {{ $data->tanggal_lahir }}</td>
                                <td>
                                    <a href="/guru/form/{{ $data->id_guru }}" class="btn btn-warning btn-xs" type="button"><i class="fa fa-pencil"></i></a>
                                    <a href="/guru/delete/{{ $data->id_guru }}" class="btn btn-danger btn-xs" type="button"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$gurus->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection