@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-university"></i> Guru</h1>
            <p>Form Guru</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">{{ isset($guru) ? 'Sunting' : 'Tambah' }} Guru</h3>
                @if ($errors->any())
                <div class="alert alert-dismissible alert-danger">
                    <button class="close" type="button" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(isset($guru))
                <form method="POST" action="/guru/{{ $guru['id_guru'] }}">
                <input type="hidden" name="_method" value="PUT">
                @else
                <form method="POST" action="/guru">
                @endif
                    {{ csrf_field() }}
                    <div class="tile-body">
                        <div class="form-group row">
                            <label class="control-label col-md-3">NIP / ID Guru</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="id_guru" @isset($guru['id_guru']) value="{{ $guru['id_guru'] }}" @endisset required>
                            </div> 
                        </div>
                    </div>
                    <div class="tile-body">
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Guru</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="nama_guru" @isset($guru['nama_guru']) value="{{ $guru['nama_guru'] }}" @endisset required>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3">JNS Kelamin</label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input @isset($guru['jenis_kelamin']) @if($guru['jenis_kelamin'] == 'L') checked @endif @endisset class="form-check-input" type="radio" name="jenis_kelamin" value="L" checked>Laki-laki
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input @isset($guru['jenis_kelamin']) @if($guru['jenis_kelamin'] == 'P') checked @endif @endisset class="form-check-input" type="radio" name="jenis_kelamin" value="P" >Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3">Tempat Lahir</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="tempat_lahir" @isset($guru['tempat_lahir']) value="{{ $guru['tempat_lahir'] }}" @endisset required>
                        </div> 
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3">Tanggal Lahir</label>
                        <div class="col-md-8">
                        <input class="form-control" name="tanggal_lahir" id="demoDate" type="text" required>
                        </div> 
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3">Alamat</label>
                        <div class="col-md-8">
                        <textarea class="form-control" name="alamat" rows="3" required>@isset($guru['alamat']){{ $guru['alamat'] }}@endisset</textarea>
                        </div> 
                    </div>
               
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/plugins/select2.min.js"></script>
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    
    @if( isset($guru['tanggal_lahir']) )
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).datepicker("update", "{{ $guru['tanggal_lahir'] }}");
        </script>
    @else
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                todayHighlight: true
            });
        </script>
    @endif
@endsection