@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-rocket"></i> Jurusan</h1>
            <p>Beranda Jurusan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <!-- tabel -->
        <div class="col-md-12">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Jurusan</h3></div>
                    <div class="col-lg-6"><a href="/jurusan/form" class="btn btn-primary pull-right" type="button">Tambah</a></div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Jurusan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($jurusans as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->nama_jurusan }}</td>
                                    <td>
                                        <a href="/jurusan/form/{{ $data->id_jurusan }}" class="btn btn-warning btn-xs" type="button"><i class="fa fa-pencil"></i></a>
                                        <a href="/jurusan/delete/{{ $data->id_jurusan }}" class="btn btn-danger btn-xs" type="button"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$jurusans->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection