@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-rocket"></i> Jurusan</h1>
            <p>Beranda Jurusan</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <!-- tabel -->
        

        <!-- form -->
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">Register</h3>
                @if ($errors->any())
                <div class="alert alert-dismissible alert-danger">
                    <button class="close" type="button" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(isset($id_jurusan))
                <form method="POST" action="/jurusan/{{$id_jurusan}}">
                <input type="hidden" name="_method" value="PUT">
                @else
                <form method="POST" action="/jurusan">
                @endif
                    {{ csrf_field() }}
                    <div class="tile-body">
                        <div class="form-group row">
                        <label class="control-label col-md-3">Nama Jurusan</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="nama_jurusan" @isset($nama_jurusan) value="{{ $nama_jurusan }}" @endisset required>
                        </div>
                    </div>
               
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection