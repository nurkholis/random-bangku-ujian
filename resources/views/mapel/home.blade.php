@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-book"></i> Mapel</h1>
            <p>Beranda Siswa</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <!-- tabel -->
        <div class="col-md-12">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Mapel</h3></div>
                    <div class="col-lg-6">
                        <a href="/mapel/form" class="btn btn-primary pull-right">Tambah</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Mata Pelajaran</th>
                            <th>Jurusan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($mapel as $data)
                            <tr>
                                <td>{{ $data->nama_mapel }}</td>
                                <td>{{ $data->nama_jurusan }}</td>
                                <td>
                                    <a href="/siswa/form/{{ $data->id_mapel }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a href="/siswa/delete/{{ $data->id_mapel }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$mapel->links()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection