@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-graduation-cap"></i> Siswa</h1>
            <p>Form Siswa</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/mapel">Mapel</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">{{ isset($mapel) ? 'Sunting' : 'Tambah' }} Mapel</h3>
                @if ($errors->any())
                <div class="alert alert-dismissible alert-danger">
                    <button class="close" type="button" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(isset($mapel['id_mapel']))
                    <form method="POST" action="/mapel/{{ $mapel['id_mapel'] }}">
                    <input type="hidden" name="_method" value="PUT">
                @else
                    <form method="POST" action="/mapel">
                @endif
                    {{ csrf_field() }}
                    <div class="tile-body">
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Jurusan</label>
                            <div class="col-md-8">
                            <select class="form-control" name="id_jurusan">
                                @if (isset($mapel))
                                    @foreach ($jurusans as $data)
                                    <option value="{{ $data->id_jurusan }}" {{ $data->id_jurusan == $mapel['id_jurusan'] ? 'selected' : '' }} >{{ $data->nama_jurusan }}</option>
                                    @endforeach
                                @else
                                    @foreach ($jurusans as $data)
                                    <option value="{{ $data->id_jurusan }}">{{ $data->nama_jurusan }}</option>
                                    @endforeach
                                @endif
                            </select>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Mata Pelajaran</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="nama_mapel" @isset($mapel['nama_mapel']) value="{{ $mapel['nama_mapel'] }}" @endisset required>
                            </div> 
                        </div>
                    </div>
               
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
    
@section('js')
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/plugins/select2.min.js"></script>
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    
    @if( isset($siswa['tanggal_lahir']) )
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).datepicker("update", "{{ $siswa['tanggal_lahir'] }}");
        </script>
    @else
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                todayHighlight: true
            });
        </script>
    @endif
@endsection