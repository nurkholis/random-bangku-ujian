@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-cubes"></i> Random</h1>
            <p>Form Guru</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/random">Random</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">{{ isset($random) ? 'Sunting' : 'Tambah' }} Random</h3>
                @if ($errors->any())
                <div class="alert alert-dismissible alert-danger">
                    <button class="close" type="button" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(isset($random))
                <form method="POST" action="/random/{{ $random['id_random'] }}">
                <input type="hidden" name="_method" value="PUT">
                @else
                <form method="POST" action="/random">
                @endif
                    {{ csrf_field() }}
                    <div class="tile-body">
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Random</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="nama_random" @isset($random['nama_random']) value="{{ $random['nama_random'] }}" @endisset required>
                            </div> 
                        </div>
                    </div>
                    @if(isset($random))
                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Random</label>
                            <div class="col-md-8">
                            <input class="form-control" name="tanggal_random" id="demoDate" type="text" required>
                            </div> 
                        </div>
                    @endif
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/plugins/select2.min.js"></script>
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    
    @if( isset($random['tanggal_random']) )
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).datepicker("update", "{{ $random['tanggal_random'] }}");
        </script>
    @endif
@endsection