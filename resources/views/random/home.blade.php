@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-cubes"></i> Random</h1>
            <p>Beranda Random</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <!-- tabel -->
        <div class="col-md-12">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Random</h3></div>
                    <div class="col-lg-6"><a href="/random/form" class="btn btn-primary pull-right">Tambah</a></div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nama Random</th>
                            <th>Tanggal Random</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($randoms as $data)
                            <tr>
                                <td>{{ $data->nama_random }}</td>
                                <td>{{ $data->tanggal_random }}</td>
                                <td>
                                    <a href="/random/form/{{ $data->id_random }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a href="/random/delete/{{ $data->id_random }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                    <a href="/random/item/{{ $data->id_random }}" class="btn btn-primary btn-xs"><i class="fa fa-cog"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$randoms->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection