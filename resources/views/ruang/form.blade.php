@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-cube"></i> Ruang</h1>
            <p>Form Rung</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/ruangan">Ruang</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">{{ isset($ruang) ? 'Sunting' : 'Tambah' }} Ruang</h3>
                @if ($errors->any())
                <div class="alert alert-dismissible alert-danger">
                    <button class="close" type="button" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(isset($ruang))
                <form method="POST" action="/ruang/{{ $ruang['id_ruang'] }}">
                <input type="hidden" name="_method" value="PUT">
                @else
                <form method="POST" action="/ruang">
                @endif
                    {{ csrf_field() }}
                    <div class="tile-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <label class="control-label col-md-6">Untuk Kelas</label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="id_kelas_1">
                                                @if (isset($ruang))
                                                    @foreach ($kelass as $data)
                                                    <option value="{{ $data->id_kelas }}" {{ $data->id_kelas == $ruang['id_kelas_1'] ? 'selected' : '' }} >{{ $data->nama_kelas }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($kelass as $data)
                                                    <option value="{{ $data->id_kelas }}">{{ $data->nama_kelas }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5">Kapasitas</label>
                                        <div class="col-md-5">
                                            <input class="form-control" type="number" name="kapasitas_kelas_1" @isset($ruang['kapasitas_kelas_1']) value="{{ $ruang['kapasitas_kelas_1'] }}" @endisset required>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <label class="control-label col-md-6">dan Kelas</label>
                                        <div class="col-md-5">
                                            <select class="form-control" name="id_kelas_2">
                                                @if (isset($ruang))
                                                    @foreach ($kelass as $data)
                                                    <option value="{{ $data->id_kelas }}" {{ $data->id_kelas == $ruang['id_kelas_2'] ? 'selected' : '' }} >{{ $data->nama_kelas }}</option>
                                                    @endforeach
                                                @else
                                                    @foreach ($kelass as $data)
                                                    <option value="{{ $data->id_kelas }}">{{ $data->nama_kelas }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5">Kapasitas</label>
                                        <div class="col-md-5">
                                            <input class="form-control" type="number" name="kapasitas_kelas_2" @isset($ruang['kapasitas_kelas_2']) value="{{ $ruang['kapasitas_kelas_2'] }}" @endisset required>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Ruang</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="nama_ruang" @isset($ruang['nama_ruang']) value="{{ $ruang['nama_ruang'] }}" @endisset required>
                            </div> 
                        </div>
                    </div>
               
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection