@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-cube"></i> Ruang</h1>
            <p>Beranda Ruang</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">

            <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Ruang</h3></div>
                    <div class="col-lg-6"><a href="/ruang/form" class="btn btn-primary pull-right">Tambah</a></div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nama Ruang</th>
                            <th>Untuk Kelas</th>
                            <th>dan Kelas</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($ruangs as $data)
                            <tr>
                                <td>{{ $data->nama_ruang }}</td>
                                <td>{{ $data->nama_kelas_1 }} {{ $data->kapasitas_kelas_1 }} Siswa</td>
                                <td>{{ $data->nama_kelas_2 }} {{ $data->kapasitas_kelas_2 }} Siswa</td>
                                <td>
                                    <a href="/ruang/form/{{ $data->id_ruang }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a href="/ruang/delete/{{ $data->id_ruang }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">

            <div class="tile">
                <div class="row">
                    <div class="col-lg-12"><h3 class="tile-title">Alokasi siswa perkelas</h3></div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Kelas</th>
                            <th>Jumlah Siswa</th>
                            <th>Terprogram</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($kelass as $data)
                            <tr>
                                <td>{{ $data->nama_kelas }}</td>
                                <td>{{ $data->jumlah_siswa }}</td>
                                @if(!$data->terpakai_1 && !$data->terpakai_2)
                                    <td>0</td>
                                @else   
                                    <td>{{ $data->terpakai_1 }} {{ $data->terpakai_2 }}</td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection