<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/font-awesome/font-awesome.css">
    @yield('css')
    <title>Random Bangku Ujian</title>
  </head>
  <body class="app sidebar-mini">

    <header class="app-header"><a class="app-header__logo" href="index.html">Random Ujian</a>
      <a class="app-sidebar__toggle" href="#" data-toggle="sidebar"></a>
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <!-- <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li> -->
            <li><a class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user text-center">
        <div class="text-center">
          <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
          <p class="app-sidebar__user-designation">Administrator</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="/"><i class="app-menu__icon fa fa-home"></i><span class="app-menu__label">Beranda</span></a></li>
        <li><a class="app-menu__item" href="/jurusan"><i class="app-menu__icon fa fa-rocket"></i><span class="app-menu__label">Jurusan</span></a></li>
        <li><a class="app-menu__item" href="/siswa"><i class="app-menu__icon fa fa-graduation-cap"></i><span class="app-menu__label">Siswa</span></a></li>
        <li><a class="app-menu__item" href="/kelas"><i class="app-menu__icon fa fa-university"></i><span class="app-menu__label">Kelas</span></a></li>
        <li><a class="app-menu__item" href="/guru"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Guru</span></a></li>
        <li><a class="app-menu__item" href="/ruang"><i class="app-menu__icon fa fa-cube"></i><span class="app-menu__label">Ruang</span></a></li>
        <li><a class="app-menu__item" href="/random"><i class="app-menu__icon fa fa-cubes"></i><span class="app-menu__label">Random</span></a></li>
      </ul>
    </aside>
    <main class="app-content">
      @yield('content')
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="/js/plugins/pace.min.js"></script>
    @yield('js')
  </body>
</html>