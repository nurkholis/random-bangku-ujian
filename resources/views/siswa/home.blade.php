@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-graduation-cap"></i> Siswa</h1>
            <p>Beranda Siswa</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <!-- tabel -->
        <div class="col-md-12">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Siswa</h3></div>
                    <div class="col-lg-6">
                        <a href="/siswa/form" class="btn btn-primary pull-right">Tambah</a>
                        <button class="btn btn-warning pull-right" data-toggle="modal" data-target="#exampleModal">Export import</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>NIS</th>
                            <th>Nama</th>
                            <th>Jurusan</th>
                            <th>Kelas</th>
                            <th>JNS Kelamin</th>
                            <th>Alamat</th>
                            <th>Lahir</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($siswas as $data)
                            <tr>
                                <td>{{ $data->nis }}</td>
                                <td>{{ $data->nama_siswa }}</td>
                                <td>{{ $data->nama_jurusan }}</td>
                                <td>{{ $data->nama_kelas }}</td>
                                <td>{{ $data->jenis_kelamin }}</td>
                                <td>{{ $data->alamat }}</td>
                                <td>{{ $data->tempat_lahir }}, {{ $data->tanggal_lahir }}</td>
                                <td>
                                    <a href="/siswa/form/{{ $data->nis }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a href="/siswa/delete/{{ $data->nis }}" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        {{$siswas->links()}}
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Export/Import Data Siswa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form method="POST" action="{{ route('import') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="tile-body">
                                <div class="form-group row">
                                    <label class="control-label col-md-3">Export</label>
                                    <div class="col-md-8">
                                        <a href="{{ route('export')  }}" class="btn btn-primary">Export</a>
                                    </div> 
                                </div>
                            </div>

                            <div class="tile-body">
                                <div class="form-group row">
                                    <label class="control-label col-md-3">Import</label>
                                    <div class="col-md-8">
                                        <input class="form-control" type="file" id="file" name="file" required>
                                    </div> 
                                </div>
                            </div>

                            <div class="tile-body">
                                <div class="form-group row">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-8">
                                        <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                                    </div> 
                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
@endsection