@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-graduation-cap"></i> Siswa</h1>
            <p>Form Siswa</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="tile">
                <h3 class="tile-title">{{ isset($kelass) ? 'Sunting' : 'Tambah' }} Siswa</h3>
                @if ($errors->any())
                <div class="alert alert-dismissible alert-danger">
                    <button class="close" type="button" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(isset($siswa['nis']))
                    <form method="POST" action="/siswa/{{ $siswa['nis'] }}">
                    <input type="hidden" name="_method" value="PUT">
                @else
                    <form method="POST" action="/siswa">
                @endif
                    {{ csrf_field() }}
                    <div class="tile-body">
                        
                         <div class="form-group row">
                            <label class="control-label col-md-3">NIS</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="nis" @isset($siswa['nis']) value="{{ $siswa['nis'] }}" @endisset required>
                            </div> 
                        </div>
                        
                        <div class="form-group row">
                            <label class="control-label col-md-3">Kelas</label>
                            <div class="col-md-8">
                            <select class="form-control" name="id_kelas">
                                @if (isset($siswa))
                                    @foreach ($kelass as $data)
                                        <option value="{{ $data->id_kelas }}" {{ $data->id_kelas == $siswa['id_kelas'] ? 'selected' : '' }} >{{ $data->nama_kelas }}</option>
                                    @endforeach
                                @else
                                    @foreach ($kelass as $data)
                                        <option value="{{ $data->id_kelas }}">{{ $data->nama_kelas }}</option>
                                    @endforeach
                                @endif
                            </select>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Nama Siswa</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="nama_siswa" @isset($siswa['nama_siswa']) value="{{ $siswa['nama_siswa'] }}" @endisset required>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">JNS Kelamin</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input @isset($siswa['jenis_kelamin']) @if($siswa['jenis_kelamin'] == 'L') checked @endif @endisset class="form-check-input" type="radio" name="jenis_kelamin" value="L" checked>Laki-laki
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input @isset($siswa['jenis_kelamin']) @if($siswa['jenis_kelamin'] == 'P') checked @endif @endisset class="form-check-input" type="radio" name="jenis_kelamin" value="P" >Perempuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="tempat_lahir" @isset($siswa['tempat_lahir']) value="{{ $siswa['tempat_lahir'] }}" @endisset required>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-8">
                            <input class="form-control" name="tanggal_lahir" id="demoDate" type="text" required>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Alamat</label>
                            <div class="col-md-8">
                            <textarea class="form-control" name="alamat" rows="3" required>@isset($siswa['alamat']){{ $siswa['alamat'] }}@endisset</textarea>
                            </div> 
                        </div>
                    </div>
               
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
    
@section('js')
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/plugins/select2.min.js"></script>
    <script type="text/javascript" src="/js/plugins/bootstrap-datepicker.min.js"></script>
    
    @if( isset($siswa['tanggal_lahir']) )
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).datepicker("update", "{{ $siswa['tanggal_lahir'] }}");
        </script>
    @else
        <script type="text/javascript">
            $('#demoDate').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                todayHighlight: true
            });
        </script>
    @endif
@endsection