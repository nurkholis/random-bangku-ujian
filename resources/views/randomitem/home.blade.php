@extends('layouts.master')
@section('css')
@endsection
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-cube"></i> Random {{$random->nama_random}}</h1>
            <p>Beranda Random Item</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/random">Random</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-dismissible alert-danger">
                <button class="close" type="button" data-dismiss="alert">×</button>
                <h4>{{$errors->first()}}</h4>
            </div>
        @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-6"><h3 class="tile-title">Data Random</h3></div>
                    @if( !$status_acak )
                        <div class="col-lg-6"><a href="/random/item/start/{{ $random->id_random }}" class="btn btn-primary pull-right">Mulai Random</a></div>
                    @else
                        <div class="col-lg-6"><a href="/random/item/destroy/{{ $random->id_random }}" class="btn btn-danger pull-right">Hapus Random</a></div>
                    @endif
                </div>
                <div class="table-responsive">
                    @if( $status_acak )
                        <table class="table" id="tabel">
                            <thead>
                            <tr>
                                <th>Nama Ruang</th>
                                <th>Nama Guru</th>
                                <th>Kapasitas</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($ruangs as $data)
                                <tr>
                                    <td>{{ $data->nama_ruang }}</td>
                                    <td>{{ $data->nama_guru }}</td>
                                    <td>{{ $data->kapasitas }}</td>
                                    <td>
                                        <a href="/random/item/show/{{ $random->id_random }}/{{ $data->id_ruang }}" class="btn btn-warning btn-xs"><i class="fa fa-eye"></i></a>
                                        <!-- <button id="btn-print" guru="{{ $data->nama_guru }}" ruang="{{ $data->nama_ruang }}" path="/{{ $random->id_random }}/{{ $data->id_ruang }}"
                                        class="btn btn-primary btn-xs"><i class="fa fa-print"></i></button> -->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-12" style="display:none" id="print">
          <div class="tile">
                <div class="row">
                    <div class="col-lg-12 text-center" ><h3 class="tile-title" id="ruang">Ruang </h3></div>
                    <div class="col-lg-12 text-center" ><h5 id="guru">Pengawas </h5></div>
                </div>
                <div class="table-responsive">
                    <table class="table" id="tabel">
                        <thead>
                            <tr>
                                <th>Nama Siswa</th>
                                <th>Nama Bangku</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>Paraf</th>
                            </tr>
                        </thead>
                        <tbody id="tabel-body-print">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@section('js')
    <script>
        $('#tabel').on( "click", '#btn-print', function(){
            var path = $(this).attr('path');
            var ruang = $(this).attr('ruang');
            var guru = $(this).attr('guru');
            $('#ruang').html(ruang);
            $('#guru').html('Pengawas ' + guru);
            console.log(path);
            $.ajax({
               type: 'GET',
               url: '/random/item/print'+path,
               success: function(data){
                    console.log(data);
                    $('#tabel-body-print').html(data);
                    printdiv('print');
               }
            });
        }); 

        function printdiv(printpage)
        {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr+newstr+footstr;
            location.reload();
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
        
    </script>
@endsection