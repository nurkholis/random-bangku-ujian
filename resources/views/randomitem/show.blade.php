@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-cube"></i> {{$random->nama_random}}</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/random">Random</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="row" >
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" onclick="printdiv('print')">Cetak</button>
                    </div>
                </div>
            </div>
            <div class="tile" id="print">
                <div class="row" >
                    <div class="col-md-12">
                        <h5 class="text-center"> Ruang {{ $ruang->nama_ruang }}</h5>
                        <br>
                    </div>
                    <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3 col-xl-3">
                        @foreach($random_ruang as $data)
                            @if($data->bangku == 1 || $data->bangku == 5 || $data->bangku == 9 || $data->bangku == 13 || $data->bangku == 17)
                            <div class="denah">
                                <h6>{{ $data->bangku }}</h6>
                                <h6>{{ $data->nama_siswa }}</h6>
                                <h6>{{ $data->nama_kelas }}</h6>
                            </div>
                            @endif()
                        @endforeach
                    </div>
                    <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3 col-xl-3">
                        @foreach($random_ruang as $data)
                            @if($data->bangku == 2 || $data->bangku == 6 || $data->bangku == 10 || $data->bangku == 14 || $data->bangku == 18)
                            <div class="denah">
                                <h6>{{ $data->bangku }}</h6>
                                <h6>{{ $data->nama_siswa }}</h6>
                                <h6>{{ $data->nama_kelas }}</h6>
                            </div>
                            @endif()
                        @endforeach
                    </div>
                    <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3 col-xl-3">
                        @foreach($random_ruang as $data)
                            @if($data->bangku == 3 || $data->bangku == 7 || $data->bangku == 11 || $data->bangku == 15 || $data->bangku == 19)
                            <div class="denah">
                                <h6>{{ $data->bangku }}</h6>
                                <h6>{{ $data->nama_siswa }}</h6>
                                <h6>{{ $data->nama_kelas }}</h6>
                            </div>
                            @endif()
                        @endforeach
                    </div>
                    <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3 col-xl-3">
                        @foreach($random_ruang as $data)
                            @if($data->bangku == 4 || $data->bangku == 8 || $data->bangku == 12 || $data->bangku == 16 || $data->bangku == 20)
                            <div class="denah">
                                <h6>{{ $data->bangku }}</h6>
                                <h6>{{ $data->nama_siswa }}</h6>
                                <h6>{{ $data->nama_kelas }}</h6>
                            </div>
                            @endif()
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function printdiv(printpage)
        {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            var newstr = document.all.item(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr+newstr+footstr;
            location.reload();
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>
@endsection