@extends('layouts.master')
@section('content')
    <div class="app-title">
        <div>
            <h1><i class="fa fa-home"></i> Beranda</h1>
            <p>Beranda</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <!-- <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="/">Dashboard</a></li> -->
        </ul>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="widget-small primary coloured-icon"><i class="icon fa fa-rocket fa-3x"></i>
            <div class="info">
                <h4>Jurusan</h4>
                <p><b>{{$jumlah['jurusan']}}</b></p>
            </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small info coloured-icon"><i class="icon fa fa-graduation-cap fa-3x"></i>
            <div class="info">
                <h4>Siswa</h4>
                <p><b>{{$jumlah['siswa']}}</b></p>
            </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small warning coloured-icon"><i class="icon fa fa-university fa-3x"></i>
            <div class="info">
                <h4>Kelas</h4>
                <p><b>{{ $jumlah['kelas'] }}</b></p>
            </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="widget-small danger coloured-icon"><i class="icon fa fa-user fa-3x"></i>
            <div class="info">
                <h4>Guru</h4>
                <p><b>{{ $jumlah['guru'] }}</b></p>
            </div>
            </div>
        </div>
    </div>
@endsection